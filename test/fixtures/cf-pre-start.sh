#!/bin/bash
if [[ -z "$appname" ]]
then
  echo "[ERROR] appname env not passed"
  exit 1
fi

if [[ -z "$tmpappname" ]]
then
  echo "[ERROR] tmpappname env not passed"
  exit 1
fi

echo "pre-start hook called for $appname/$tmpappname"
